<?php

require_once __DIR__ . '/../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
const QUEUE_NAME = 'worker.queue';
$channel->queue_declare(QUEUE_NAME, false, false, false, false);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$callback = function(AMQPMessage $msg) {
    $string = $msg->body;
    echo " [o] Received: " . $string . "\n";
    sleep(substr_count($string, '.'));
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    echo " [x] Processed: " . $string . "\n";
};

$channel->basic_consume(QUEUE_NAME, '', false, false, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}
