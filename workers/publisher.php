<?php
require_once __DIR__ . '/../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
const QUEUE_NAME = 'worker.queue';
$channel->queue_declare(QUEUE_NAME, false, false, false, false);
$startId = 1;
while (true) {
    $input = 'Work ' . $startId++ . str_repeat('.', rand(0, 10));
    $msg = new AMQPMessage($input);
    $channel->basic_publish($msg, '', QUEUE_NAME);

    echo " [x] Sent '$input'\n";
    sleep(1);
}

$closeConnection = function ($signo) use ($channel, $connection) {
    $channel->close();
    $connection->close();

    echo " [x_x] Closed!";
};

pcntl_signal(SIGTERM, $closeConnection);
pcntl_signal(SIGHUP,  $closeConnection);
pcntl_signal(SIGUSR1, $closeConnection);

