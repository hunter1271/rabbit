<?php
require_once __DIR__ . '/../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

const EXCHANGE_NAME = 'service.events';

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->exchange_declare(EXCHANGE_NAME, 'fanout', false, false, false);
$startId = 1;

while (true) {
    $input = 'Event #' . $startId++;
    $msg = new AMQPMessage($input);
    $channel->basic_publish($msg, EXCHANGE_NAME);

    echo " [x] Emited: '$input'\n";
    sleep(1);
}

$closeConnection = function ($signo) use ($channel, $connection) {
    $channel->close();
    $connection->close();

    echo " [x_x] Closed!";
};

pcntl_signal(SIGTERM, $closeConnection);
pcntl_signal(SIGHUP,  $closeConnection);
pcntl_signal(SIGUSR1, $closeConnection);
